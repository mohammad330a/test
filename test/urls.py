from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('poll/', include('poll.urls')),
    path('admin/', admin.site.urls),
]

admin.site.site_header = "my test admin"
admin.site.site_title = "testing"
admin.site.index_title = "i guess we made it"
